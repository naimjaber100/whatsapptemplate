﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Whatsapp.Template.Client.Requests;

namespace Whatsapp.Template.UnitTesting.Helpers
{
    public class TemplateHelper
    {
        private readonly TemplateRequestInfo templateRequestInfo;
        private readonly TemplateGetRequestInfo templateGetRequestInfo;
        private readonly TemplateDeleteRequestInfo templateDeleteRequestInfo;

        public TemplateHelper()
        {
            templateRequestInfo = new TemplateRequestInfo()
            {
                
            };

            templateGetRequestInfo = new TemplateGetRequestInfo()
            {
                AccountId = 5,
                Name = "name",
                TenantId = 1,
                Status = ""
            };

            templateDeleteRequestInfo = new TemplateDeleteRequestInfo()
            {
                AccountId = 5,
                Name = "name",
                TenantId = 1
            };
        }

        public TemplateRequestInfo GetTemplateRequestInfo()
        {
            return templateRequestInfo;
        }

        public TemplateGetRequestInfo GetRequestInfo()
        {
            return templateGetRequestInfo;
        }

        public TemplateDeleteRequestInfo GetTemplateDeleteRequestInfo()
        {
            return templateDeleteRequestInfo;
        }
    }
}
