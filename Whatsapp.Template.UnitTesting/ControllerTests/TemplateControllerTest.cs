﻿using Common.Business.Common;
using Common.Client.Response;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Whatsapp.Template.Business.Contracts.Service;
using Whatsapp.Template.Client.Requests;
using Whatsapp.Template.Controllers;
using Whatsapp.Template.UnitTesting.Extensions;
using Whatsapp.Template.UnitTesting.Helpers;
using Xunit;

namespace Whatsapp.Template.UnitTesting.ControllerTests
{
    public class TemplateControllerTest
    {
        #region Fields
        private readonly TemplateController templateController;
        private readonly Mock<ITemplateService> _mockService;
        private readonly TemplateHelper templateHelper;

        private readonly Mock<IJsonAdapter> _mockJsonAdapter;
        private readonly Mock<ILogAdapter> _mockLogAdapter;

        private readonly Dictionary<string, string> httpRequestHeaders;
        private readonly Dictionary<string, string> httpRequestUserClaims;

        public string _tenantKey = "1";

        #endregion

        #region Constructors

        public TemplateControllerTest()
        {
            _mockService = new Mock<ITemplateService>();
            _mockJsonAdapter = new Mock<IJsonAdapter>();
            _mockLogAdapter = new Mock<ILogAdapter>();

            templateHelper = new TemplateHelper();

            httpRequestHeaders = new Dictionary<string, string>
            {
                { "tenant-key", _tenantKey }
            };

            httpRequestUserClaims = new Dictionary<string, string>
            {
                { "ExternalID", "1" }
            };

            templateController = new TemplateController(_mockLogAdapter.Object, _mockJsonAdapter.Object, _mockService.Object)
                .CreateFakeHttpContext(httpRequestHeaders, httpRequestUserClaims);
        }

        #endregion

        #region Tests

        [Fact]

        public void Template_Add_Return_Ok()
        {
            //Arrange

            var mockedObj = new Mock<TemplateRequestInfo>();

            _mockService.Setup(ser => ser.AddTemplate(It.IsAny<TemplateRequestInfo>()))
                .Returns(new ClientResponse { ResponseCode = System.Net.HttpStatusCode.OK });
            //Act

            var result = templateController.AddTemplate(mockedObj.Object);

            //Assert

            AssertWithSuccess(result);
            _mockService.Verify(ser => ser.AddTemplate(It.IsAny<TemplateRequestInfo>()), Times.Once);
        }

        [Fact]

        public void Template_Add_Return_Null_WhenTenantKeyIsNotSubmitted()
        {
            //Arrange
            var _controller = new TemplateController(_mockLogAdapter.Object, _mockJsonAdapter.Object, _mockService.Object)
              .CreateFakeHttpContext(null, httpRequestUserClaims);


            var mockedObj = new Mock<TemplateRequestInfo>();

            _mockService.Setup(ser => ser.AddTemplate(It.IsAny<TemplateRequestInfo>()));

            //Act

            var result = _controller.AddTemplate(mockedObj.Object);

            //Assert

            Assert.IsType<ActionResult<ClientResponse>>(result);
            Assert.Null(result?.Value);

            _mockService.Verify(ser => ser.AddTemplate(It.IsAny<TemplateRequestInfo>()), Times.Never);
        }

        [Fact]

        public void Template_Get_TypeBody_Return_Ok()
        {
            //Arrange

            _mockService.Setup(ser => ser.GetTemplateTypeBody())
                .Returns(new ClientResponse { ResponseCode = System.Net.HttpStatusCode.OK });
            //Act

            var result = templateController.GetTemplateTypeBody();

            //Assert

            AssertWithSuccess(result);
            _mockService.Verify(ser => ser.GetTemplateTypeBody(), Times.Once);
        }

        [Fact]

        public void Template_Get_HeaderMediaType_Return_Ok()
        {
            //Arrange

            _mockService.Setup(ser => ser.GetTemplateHeaderMediaType())
                .Returns(new ClientResponse { ResponseCode = System.Net.HttpStatusCode.OK });
            //Act

            var result = templateController.GetTemplateHeaderMediaType();

            //Assert

            AssertWithSuccess(result);
            _mockService.Verify(ser => ser.GetTemplateHeaderMediaType(), Times.Once);
        }

        [Fact]

        public void Template_Get_ButtonType_Return_Ok()
        {
            //Arrange

            _mockService.Setup(ser => ser.GetTemplateButtonType())
                .Returns(new ClientResponse { ResponseCode = System.Net.HttpStatusCode.OK });
            //Act

            var result = templateController.GetTemplateButtonType();

            //Assert

            AssertWithSuccess(result);
            _mockService.Verify(ser => ser.GetTemplateButtonType(), Times.Once);
        }

        [Fact]

        public void Template_Get_Category_Return_Ok()
        {
            //Arrange

            _mockService.Setup(ser => ser.GetCategory())
                .Returns(new ClientResponse { ResponseCode = System.Net.HttpStatusCode.OK });
            //Act

            var result = templateController.GetCategory();

            //Assert

            AssertWithSuccess(result);
            _mockService.Verify(ser => ser.GetCategory(), Times.Once);
        }

        [Fact]

        public void Template_Get_Language_Return_Ok()
        {
            //Arrange

            _mockService.Setup(ser => ser.GetLanguage())
                .Returns(new ClientResponse { ResponseCode = System.Net.HttpStatusCode.OK });
            //Act

            var result = templateController.GetLanguage();

            //Assert

            AssertWithSuccess(result);
            _mockService.Verify(ser => ser.GetLanguage(), Times.Once);
        }

        [Fact]

        public void Template_Get_Return_Ok()
        {
            //Arrange

            var testObj = templateHelper.GetRequestInfo();

            _mockService.Setup(ser => ser.GetTemplate(It.IsAny<TemplateGetRequestInfo>()))
                .Returns(new ClientResponse { ResponseCode = System.Net.HttpStatusCode.OK });
            //Act

            var result = templateController.GetTemplate(testObj);

            //Assert

            AssertWithSuccess(result);
            _mockService.Verify(ser => ser.GetTemplate(It.IsAny<TemplateGetRequestInfo>()), Times.Once);
        }

        [Fact]

        public void Template_Get_Return_BadResult_WhenTenantKeyIsNotSubmitted()
        {
            //Arrange

            var _controller = new TemplateController(_mockLogAdapter.Object, _mockJsonAdapter.Object, _mockService.Object)
            .CreateFakeHttpContext(null, httpRequestUserClaims);

            var testObj = templateHelper.GetRequestInfo();

            _mockService.Setup(ser => ser.GetTemplate(It.IsAny<TemplateGetRequestInfo>()));
            //Act

            var result = _controller.GetTemplate(testObj);

            //Assert

            Assert.IsType<ActionResult<ClientResponse>>(result);
            Assert.Null(result?.Value);
            _mockService.Verify(ser => ser.GetTemplate(It.IsAny<TemplateGetRequestInfo>()), Times.Never);
        }

        [Fact]

        public void Template_Get_Return_BadResult_WhenAccountIdIsNotSubmitted()
        {
            //Arrange

            var _controller = new TemplateController(_mockLogAdapter.Object, _mockJsonAdapter.Object, _mockService.Object)
            .CreateFakeHttpContext(httpRequestHeaders, null);

            var testObj = templateHelper.GetRequestInfo();

            _mockService.Setup(ser => ser.GetTemplate(It.IsAny<TemplateGetRequestInfo>()))
                .Returns(new ClientResponse { ResponseCode = System.Net.HttpStatusCode.BadRequest});
            //Act

            var result = _controller.GetTemplate(testObj);

            //Assert

            Assert.IsType<ActionResult<ClientResponse>>(result);
            Assert.Null(result?.Value);
            _mockService.Verify(ser => ser.GetTemplate(It.IsAny<TemplateGetRequestInfo>()), Times.Never);
        }

        [Fact]

        public void Template_Delete_Return_Ok()
        {
            //Arrange

            var testObj = templateHelper.GetTemplateDeleteRequestInfo();

            _mockService.Setup(ser => ser.DeleteTemplate(It.IsAny<TemplateDeleteRequestInfo>()))
                .Returns(new ClientResponse { ResponseCode = System.Net.HttpStatusCode.OK });
            //Act

            var result = templateController.DeleteTemplate(testObj);

            //Assert

            AssertWithSuccess(result);
            _mockService.Verify(ser => ser.DeleteTemplate(It.IsAny<TemplateDeleteRequestInfo>()), Times.Once);
        }

        [Fact]

        public void Template_Delete_Return_BadResult_WhenTenantKeyIsNotSubmitted()
        {
            //Arrange

            var _controller = new TemplateController(_mockLogAdapter.Object, _mockJsonAdapter.Object, _mockService.Object)
            .CreateFakeHttpContext(null, httpRequestUserClaims);

            var testObj = templateHelper.GetTemplateDeleteRequestInfo();

            _mockService.Setup(ser => ser.DeleteTemplate(It.IsAny<TemplateDeleteRequestInfo>()));
            //Act

            var result = _controller.DeleteTemplate(testObj);

            //Assert

            Assert.IsType<ActionResult<ClientResponse>>(result);
            Assert.Null(result?.Value);
            _mockService.Verify(ser => ser.DeleteTemplate(It.IsAny<TemplateDeleteRequestInfo>()), Times.Never);
        }

        [Fact]

        public void Template_Delete_Return_BadResult_WhenSccountIdIsNotSubmitted()
        {
            //Arrange

            var _controller = new TemplateController(_mockLogAdapter.Object, _mockJsonAdapter.Object, _mockService.Object)
            .CreateFakeHttpContext(httpRequestHeaders, null);

            var testObj = templateHelper.GetTemplateDeleteRequestInfo();

            _mockService.Setup(ser => ser.DeleteTemplate(It.IsAny<TemplateDeleteRequestInfo>()));
            //Act

            var result = _controller.DeleteTemplate(testObj);

            //Assert

            Assert.IsType<ActionResult<ClientResponse>>(result);
            Assert.Null(result?.Value);
            _mockService.Verify(ser => ser.DeleteTemplate(It.IsAny<TemplateDeleteRequestInfo>()), Times.Never);
        }

        #endregion

        #region Methods

        private static void AssertWithSuccess(ActionResult<ClientResponse> result)
        {

            Assert.IsType<ActionResult<ClientResponse>>(result);
            Assert.NotNull(result?.Value);

            ClientResponse? clientResponse = result?.Value;
            Assert.Equal(System.Net.HttpStatusCode.OK, clientResponse?.ResponseCode);
        }
        #endregion
    }
}
