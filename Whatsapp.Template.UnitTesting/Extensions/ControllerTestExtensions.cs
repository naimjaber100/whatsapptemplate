﻿using Common.Business.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Whatsapp.Template.UnitTesting.Extensions
{
    public static class ControllerTestExtensions
    {

        public static T CreateFakeHttpContext<T>
            (this T controller, Dictionary<string, string>? requestHeaderKeyValues = null,
            Dictionary<string, string>? userClaimsKeyValues = null)
            where T : BaseApiController
        {
            controller.CreateHttpContext();

            var claims = new ClaimsIdentity();

            if (userClaimsKeyValues != null)
            {
                foreach (var keyValue in userClaimsKeyValues)
                {
                    claims.AddClaim(new Claim
                        (keyValue.Key,
                        keyValue.Value)
                        );
                }
            }

            var principal = new ClaimsPrincipal(claims);
            controller.ControllerContext.HttpContext.User = principal;

            if (requestHeaderKeyValues != null)
            {
                foreach (var keyValue in requestHeaderKeyValues)
                {
                    controller.HttpContext.Request.Headers[keyValue.Key] = keyValue.Value;

                }
            }


            if (controller.HttpContext.Connection.RemoteIpAddress == null)
            {
                controller.HttpContext.Connection.RemoteIpAddress = System.Net.IPAddress.Parse("127.0.0.1");
            }

            return controller;
        }


        private static T CreateHttpContext<T>(this T controller) where T : BaseApiController
        {
            if (controller.ControllerContext == null)
            {
                controller.ControllerContext = new ControllerContext();
            }

            if (controller.ControllerContext.HttpContext == null)
            {
                controller.ControllerContext.HttpContext = new DefaultHttpContext();

            }

            return controller;
        }
    }
}
