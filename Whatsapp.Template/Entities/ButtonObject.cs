﻿namespace Whatsapp.Template.Entities
{
    public class ButtonObject
    {
        public string? type { get; set; }
        public List<object>? buttons { get; set; }
    }
}
