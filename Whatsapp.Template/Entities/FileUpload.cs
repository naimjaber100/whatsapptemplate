﻿namespace Whatsapp.Template.Entities
{
    public class FileUpload
    {
        public string? AccessToken { get; set; }

        public long FileLength { get; set; }
        public string? FileType { get; set; }
        public string? FileName { get; set; }
    }
}
