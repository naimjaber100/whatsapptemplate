﻿namespace Whatsapp.Template.Entities
{
    public class Language
    {
        public int Id { get; set; }
        public string? Country { get; set; }
        public string? Code { get; set; }
    }
}
