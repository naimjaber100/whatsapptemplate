﻿namespace Whatsapp.Template.Entities
{
    public class Template
    {
        public Guid TemplateId { get; set; }
        public string? Name { get; set; }
        public string? Language { get; set; }
        public int TemplateType { get; set; }
        public string? MessageId { get; set; }
        public int AccountId { get; set; }
        public string? WabaAccount { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsApproved { get; set; }
        public string? TemplateText { get; set; }
        public string? AccessToken { get; set; }
    }
}
