﻿namespace Whatsapp.Template.Entities
{
    public class UploadSession
    {
        public string? id { get; set; }
        public string? h { get; set; }
    }
}
