﻿using Common.Business.Common;
using Common.Enums;
using Common.Extensions;
using Dapper;
using Whatsapp.Template.Client.Requests;
using Whatsapp.Template.Common.Static;
using Whatsapp.Template.Entities;
using Whatsapp.Template.Persistence.Contracts.Repositories;

namespace Whatsapp.Template.Persistence.Data.Repositories
{
    public class TemplateWhatsappRepository : WhatsappRepositoryBase<dynamic>, ITemplateWhatsappRepository
    {
        private readonly ICacheAdapter _cacheAdapter;
        private readonly IJsonAdapter _jsonAdapter;
        public TemplateWhatsappRepository(Func<CacheType, ICacheAdapter> CacheAdapter, IJsonAdapter jsonAdapter)
        {
            _cacheAdapter = CacheAdapter(CacheType.MemoryCache);
            _jsonAdapter = jsonAdapter;
        }

        #region Public Methods
        public int AddTemplate(TemplateRequestInfo templateRequestInfo)
        {
            var parameters = new DynamicParameters();
            parameters.Add("_name", templateRequestInfo.Name);
            parameters.Add("_language", templateRequestInfo.Language);
            parameters.Add("_template_type",(int) templateRequestInfo.TemplateType);
            parameters.Add("_waba_account", templateRequestInfo.WABA);
            parameters.Add("_account_id", templateRequestInfo.AccountId);
            parameters.Add("_access_token", templateRequestInfo.AccessToken);
            parameters.Add("_template_text", templateRequestInfo.TemplateText);
            var id = GetSingle<int>(Procedure.AddTemplate, parameters);


            return id;
        }

        public  int AddHeaderMediaTemplateParamter(HeaderMediaRequestInfo headerMediaRequestInfo,int TemplateId)
        {

            var example = "";
            if (headerMediaRequestInfo.example != null)
            {
                if (headerMediaRequestInfo.example.header_handle != null)
                {
                    example = headerMediaRequestInfo.example.header_handle[0];
                }

            }
            var parameters = new DynamicParameters();
            parameters.Add("_template_id", TemplateId);
            parameters.Add("_type", headerMediaRequestInfo.type.ToString());
            parameters.Add("_format", headerMediaRequestInfo.format.ToString());
            parameters.Add("_example", example);

            var id = GetSingle<int>(Procedure.AddHeaderMediaTemplateParameter, parameters);
            return (int)id;
        }

        public int AddHeaderTextTemplateParamter(HeaderTextRequestInfo headerTextRequestInfo, int TemplateId)
        {

            var example = "";
            if (headerTextRequestInfo.example != null)
            {
                if (headerTextRequestInfo.example.header_text != null)
                {
                    example = headerTextRequestInfo.example.header_text[0];
                }

            }
            var parameters = new DynamicParameters();
            parameters.Add("_template_id", TemplateId);
            parameters.Add("_type",headerTextRequestInfo.type.ToString());
            parameters.Add("_format",headerTextRequestInfo.format.ToString());
            parameters.Add("_text", headerTextRequestInfo.text);
            parameters.Add("_header_number_of_variable", headerTextRequestInfo.numberOfVariables);
            parameters.Add("_example", example);

            var id = GetSingle<int>(Procedure.AddHeaderTextTemplateParameter, parameters);
            return (int)id;
        }
        public int AddBodyTemplateParamter(BodyRequestInfo bodyRequestInfo, int TemplateId)
        {
            var example = "";
            if (bodyRequestInfo.example != null)
            {
                if(bodyRequestInfo.example.body_text != null)
                {
                    example = _jsonAdapter.Serialize( bodyRequestInfo.example.body_text[0]);
                }
                 
            }
       

            var parameters = new DynamicParameters();
            parameters.Add("_template_id", TemplateId);
            parameters.Add("_type", bodyRequestInfo.type.ToString());
            parameters.Add("_text", bodyRequestInfo.text);
            parameters.Add("_example", example);
            parameters.Add("_body_number_of_variable", bodyRequestInfo.numberOfVariables);

            var id = GetSingle<int>(Procedure.AddBodyTemplateParameter, parameters);
            return (int)id;
        }
        public int AddFooterTemplateParamter(FooterRequestInfo footerRequestInfo, int TemplateId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("_template_id", TemplateId);
            parameters.Add("_type", footerRequestInfo.type.ToString());
            parameters.Add("_text", footerRequestInfo.text);

            var id = GetSingle<int>(Procedure.AddFooterTemplateParameter, parameters);
            return (int)id;
        }
        public int AddButtonUrlTemplateParamter(ButtonUrlRequestInfo buttonUrlRequestInfo, int TemplateId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("_template_id", TemplateId);
            parameters.Add("_type", buttonUrlRequestInfo.type.ToString());
            parameters.Add("_text", buttonUrlRequestInfo.text);
            parameters.Add("_url", buttonUrlRequestInfo.url);

            var id = GetSingle<int>(Procedure.AddButtonUrlTemplateParameter, parameters);
            return (int)id;
        }

        public int AddButtonCallTemplateParamter(ButtonCallRequestInfo buttonCallRequestInfo, int TemplateId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("_template_id", TemplateId);
            parameters.Add("_type", buttonCallRequestInfo.type.ToString());
            parameters.Add("_text", buttonCallRequestInfo.text);
            parameters.Add("_phone_number", buttonCallRequestInfo.phone_number);

            var id = GetSingle<int>(Procedure.AddButtonCallTemplateParameter, parameters);
            return (int)id;
        }
        public  IEnumerable<Language> GetLanguage()
        {

            DynamicParameters dynamicParameters = new DynamicParameters();

         
            var result = GetList<Language>(Procedure.GetLanguage, dynamicParameters);

            return result;
        }

        public WhatsappAccount GetWhatsappAccount(int tenantId, int accountId, bool enableCaching = true)
        {
            if (enableCaching)
            {
                // string cacheKey = string.Format("{0}_{1}", tenantkey, channel);
                string cacheKey = string.Format(CacheKeys.WhatsappAccountKey, tenantId, accountId);


                return _cacheAdapter.Get(cacheKey, AppConstants.TemplateAccount_Cache_Time_Minutes, () =>
                {
                    return GetWhatsappAccount(tenantId, accountId);
                });
            }
            else
            {
                return GetWhatsappAccount(tenantId, accountId);
            }
        }

        #endregion Public Methods

        #region Private Methods
        private WhatsappAccount GetWhatsappAccount(int tenantId, int accountId)
        {
         
            DynamicParameters dynamicParameters = new DynamicParameters();

            dynamicParameters.Add("_account_id", accountId);
            dynamicParameters.Add("_tenant_id", tenantId);
            WhatsappAccount whatsappAccounts = GetSingle<WhatsappAccount>(Procedure.GetWhatsappAccount, dynamicParameters);

        
            return whatsappAccounts;
        }

        #endregion Private Methods

    }
}
