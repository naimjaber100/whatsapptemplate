﻿using Whatsapp.Template.Client.Requests;
using Whatsapp.Template.Entities;

namespace Whatsapp.Template.Persistence.Contracts.Repositories
{
    public interface ITemplateWhatsappRepository
    {
        int AddTemplate(TemplateRequestInfo templateRequestInfo);

      
        int AddHeaderMediaTemplateParamter(HeaderMediaRequestInfo headerMediaRequestInfo,int TemplateId );
        int AddHeaderTextTemplateParamter(HeaderTextRequestInfo headerTextRequestInfo, int TemplateId);
        int AddBodyTemplateParamter(BodyRequestInfo bodyRequestInfo, int TemplateId);
        int AddFooterTemplateParamter(FooterRequestInfo footerRequestInfo, int TemplateId);
        int AddButtonCallTemplateParamter(ButtonCallRequestInfo buttonCallRequestInfo, int TemplateId);

        int AddButtonUrlTemplateParamter(ButtonUrlRequestInfo buttonUrlRequestInfo, int TemplateId);

        IEnumerable<Language> GetLanguage();

        WhatsappAccount GetWhatsappAccount(int tenantId, int accountId, bool enableCaching = true);
    }
}
