using Com.Monty.Omni.Global.Business.Bootstrapper;
using Com.Monty.Omni.Global.Common.Configurations;
using Common.Business.Common;
using Whatsapp.Template.AutoMapper;
using Whatsapp.Template.Business.Contracts.Engine;
using Whatsapp.Template.Business.Contracts.Service;
using Whatsapp.Template.Business.Engine;
using Whatsapp.Template.Common.Configuration;
using Whatsapp.Template.Persistence.Contracts.Repositories;
using Whatsapp.Template.Persistence.Data.Repositories;

var MyAllowSpecificOrigins = "_myAllowSpecificOrigins";



var builder = WebApplication.CreateBuilder(args);

ConfigurationManager configuration = builder.Configuration; // allows both to access and to set up the config
IWebHostEnvironment environment = builder.Environment;

//services cors
builder.Services.AddCors(p => p.AddPolicy("corsapp", builder =>
{
    builder.WithOrigins("*").AllowAnyMethod().AllowAnyHeader();
}));

// Add services to the container.
//builder.Services.AddCors();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();


builder.Services.InitOmni();

ApplicationConfiguration.DbConnection = Environment.GetEnvironmentVariable("DbConnection");
ApplicationConfiguration.DbConnectionSchemaName = Environment.GetEnvironmentVariable("DbConnectionSchemaName");
ApplicationConfiguration.ConfigurationDbConnection = Environment.GetEnvironmentVariable("ConfigurationDbConnection");
ApplicationConfiguration.ConfigurationDbConnectionSchemaName = Environment.GetEnvironmentVariable("ConfigurationSchemaName");
ApplicationConfigurationWhatsapp.WhatsappDbConnection = Environment.GetEnvironmentVariable("WhatsappDbConnection");
ApplicationConfigurationWhatsapp.WhatsappConnectionSchemaName = Environment.GetEnvironmentVariable("WhatsappSchemaName");

builder.Services.AddSingleton<ITemplateEngine, TemplateEngine>();
builder.Services.AddSingleton<ITemplateService, TemplateService>();
builder.Services.AddSingleton<ITemplateWhatsappRepository, TemplateWhatsappRepository>();
builder.Services.AddSingleton<IAutoMapper, AutoMapperr>();

//builder.Services.ConfigureAuthentication();


builder.Services.ConfigureKeycloakAuthentication(environment);

var app = builder.Build();



// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseCors("corsapp");
app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
