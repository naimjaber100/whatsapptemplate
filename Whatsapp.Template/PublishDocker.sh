

echo "whatsapptemplate --- STARTING"

#cd MTSmppServer/
docker rmi sms-repo.montymobile.com/whatsapptemplate:1.0.1
rm -frd publish/
dotnet publish Whatsapp.Template.csproj -c Release -o publish
docker build -t sms-repo.montymobile.com/whatsapptemplate:1.0.1 .
docker push sms-repo.montymobile.com/whatsapptemplate:1.0.1
rm -frd publish/
cd ..
cd ..

echo "whatsapptemplate --- FINISHED"

sleep 1d