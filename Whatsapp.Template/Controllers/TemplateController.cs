﻿using Common.Business.Common;
using Common.Business.Services;
using Common.Client.Response;
using Firebase.Auth;
using Firebase.Storage;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Whatsapp.Template.Business.Contracts.Service;
using Whatsapp.Template.Client.Requests;

namespace Whatsapp.Template.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
   // [Authorize]
    public class TemplateController : BaseApiController
    {
        private readonly ITemplateService _templateService;
        public TemplateController(ILogAdapter LogAdapter,IJsonAdapter JsonAdapter, ITemplateService templateService) : base(LogAdapter, JsonAdapter)
        {
            _templateService = templateService;
        }
    

        [HttpPost]
        [Route("AddTemplate")]
        public ActionResult<ClientResponse> AddTemplate([FromBody] TemplateRequestInfo templateRequestInfo)
        {
            return ExecuteOperation(() =>
            {

                string tenantKey = IncomingRequestHeader.TenantKey ?? throw new InvalidOperationException("Invalid Tenant Id"); 
                templateRequestInfo.TenantId = Convert.ToInt32(tenantKey);

                templateRequestInfo.AccountId = int.Parse(IncomingAuthorizedUser.Claims.FirstOrDefault(c => c.Type.Equals("ExternalID"))?.Value
                ?? throw new InvalidOperationException("Invalid AccountID"));
                return _templateService.AddTemplate(templateRequestInfo);
            });
        }

        [HttpGet]
        [Route("TemplateTypeBody")]
        public ActionResult<ClientResponse> GetTemplateTypeBody()
        {
      
            return _templateService.GetTemplateTypeBody();
        }

        [HttpGet]
        [Route("TemplateHeaderMediaType")]
        public ActionResult<ClientResponse> GetTemplateHeaderMediaType()
        {

            return _templateService.GetTemplateHeaderMediaType();
        }

        [HttpGet]
        [Route("TemplateButtonType")]
        public ActionResult<ClientResponse> GetTemplateButtonType()
        {

            return _templateService.GetTemplateButtonType();
        }

        [HttpGet]
        [Route("Category")]
        public ActionResult<ClientResponse> GetCategory()
        {

            return _templateService.GetCategory();
        }

        [HttpGet]
        [Route("Language")]
        public ActionResult<ClientResponse> GetLanguage()
        {

            return _templateService.GetLanguage();
        }

        [HttpGet]
        public ActionResult<ClientResponse> GetTemplate([FromQuery] TemplateGetRequestInfo templateGetRequestInfo)
        {
            return ExecuteOperation(() =>
            {
                string tenantKey = IncomingRequestHeader.TenantKey ?? throw new InvalidOperationException("Invalid Tenant Id");
            templateGetRequestInfo.TenantId = Convert.ToInt32(tenantKey);


            templateGetRequestInfo.AccountId = int.Parse(IncomingAuthorizedUser.Claims.FirstOrDefault(c => c.Type.Equals("ExternalID"))?.Value
             ?? throw new InvalidOperationException("Invalid AccountID"));
            return _templateService.GetTemplate(templateGetRequestInfo);

            });
        }
        [HttpDelete]
        public ActionResult<ClientResponse> DeleteTemplate([FromQuery] TemplateDeleteRequestInfo templateDeleteRequestInfo)
        {
            return ExecuteOperation(() =>
            {
                logAdapter.WriteInfoLog("start delete template controller");
            string tenantKey = IncomingRequestHeader.TenantKey ?? throw new InvalidOperationException("Invalid Tenant Id");
            templateDeleteRequestInfo.TenantId = Convert.ToInt32(tenantKey);


            templateDeleteRequestInfo.AccountId = int.Parse(IncomingAuthorizedUser.Claims.FirstOrDefault(c => c.Type.Equals("ExternalID"))?.Value
             ?? throw new InvalidOperationException("Invalid AccountID"));
            return _templateService.DeleteTemplate(templateDeleteRequestInfo);

            });
        }


        [AllowAnonymous]
        [HttpPost]
        [Route("UploadFile")]
        public ActionResult<ClientResponse> UploadFile(IFormFile file)
        {
            return _templateService.UploadFile(file);

        }

        [AllowAnonymous]
        [HttpPost]
        [Route("UploadWhatsappFile")]
        public ActionResult<ClientResponse> UploadWhatsappFile(IFormFile file)
        {

            string tenantKey = IncomingRequestHeader.TenantKey ?? throw new InvalidOperationException("Invalid Tenant Id");


            int AccountId = int.Parse(IncomingAuthorizedUser.Claims.FirstOrDefault(c => c.Type.Equals("ExternalID"))?.Value
             ?? throw new InvalidOperationException("Invalid AccountID"));

            return _templateService.UploadWhatsappFile(file,Convert.ToInt32( tenantKey), AccountId);

        }
    }
}
