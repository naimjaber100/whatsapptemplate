﻿using Whatsapp.Template.Common.Configuration;

namespace Whatsapp.Template.Common.Static
{
    public class Procedure
    {
        public static readonly string AddTemplate = $"{ApplicationConfigurationWhatsapp.WhatsappConnectionSchemaName}.whatsapp_template_insert";
       
        public static readonly string AddHeaderTextTemplateParameter = $"{ApplicationConfigurationWhatsapp.WhatsappConnectionSchemaName}.whatsapp_template_header_text_insert";
       
        public static readonly string AddHeaderMediaTemplateParameter = $"{ApplicationConfigurationWhatsapp.WhatsappConnectionSchemaName}.whatsapp_template_header_media_insert";
       
        public static readonly string AddFooterTemplateParameter = $"{ApplicationConfigurationWhatsapp.WhatsappConnectionSchemaName}.whatsapp_template_footer_insert";
       
        public static readonly string AddBodyTemplateParameter = $"{ApplicationConfigurationWhatsapp.WhatsappConnectionSchemaName}.whatsapp_template_body_insert";
       
        public static readonly string AddButtonCallTemplateParameter = $"{ApplicationConfigurationWhatsapp.WhatsappConnectionSchemaName}.whatsapp_template_button_call_insert";
      
        public static readonly string AddButtonUrlTemplateParameter = $"{ApplicationConfigurationWhatsapp.WhatsappConnectionSchemaName}.whatsapp_template_button_url_insert";


        public static readonly string GetLanguage = $"{ApplicationConfigurationWhatsapp.WhatsappConnectionSchemaName}.whatsapp_language_get";
        public static readonly string GetWhatsappAccount = $"{ApplicationConfigurationWhatsapp.WhatsappConnectionSchemaName}.whatsapp_account_get";

    }
}
