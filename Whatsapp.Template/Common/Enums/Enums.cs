﻿namespace Whatsapp.Template.Common.Enums
{
    public class Enums
    {
        public enum TemplateType
        {
            Text = 1,
            Video = 2,
            Image = 3,
            Document = 4,
            TextCallURL = 5,
            TextCall = 6,
            TextURL = 7,
           // VideoreplyButtons = 8,
           // ImageReplyButtons = 9,
           // DocumentReplyButtons = 10,
           // TextReplyButtons = 11,

        }

        public enum TemplateTypeBody
        {
            HEADER = 1,
            BODY = 2,
            FOOTER = 3 ,
            BUTTON = 4
        }

        public enum TemplateHeaderMediaType
        {
            IMAGE = 1,
            VIDEO = 2,
            DOCUMENT = 3,
            TEXT = 4,
        }

        public enum TemplateButtonType
        {
            PHONE_NUMBER = 1,
            URL = 2
        }
        public enum CategoryOld1
        {
        ACCOUNT_UPDATE,
        PAYMENT_UPDATE,
        PERSONAL_FiNANCE_UPDATE,
        SHIPPING_UPDATE,
        RESERVATION_UPDATE,
        ISSUE_RESOLUTION,
        APPOINTMENT_UPDATE,
        TRANSPORTATION_UPDATE,
        TICKET_UPDATE,
        ALERT_UPDATE

        }

        public enum Category
        {
            TRANSACTIONAL,
            MARKETING,
            OTP
        }

        public enum CategoryOld
        {
            ACCOUNT_UPDATE,
            PAYMENT_UPDATE,
            PERSONAL_FiNANCE_UPDATE,
            SHIPPING_UPDATE,
            RESERVATION_UPDATE,
            ISSUE_RESOLUTION,
            APPOINTMENT_UPDATE,
            TRANSPORTATION_UPDATE,
            TICKET_UPDATE,
            ALERT_UPDATE,
            AUTO_UPDATE

        }
    }
}
