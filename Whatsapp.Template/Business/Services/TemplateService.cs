﻿using Common;
using Common.Business.Common;
using Common.Client.Response;
using Firebase.Auth;
using Firebase.Storage;
using Whatsapp.Template.Business.Contracts.Engine;
using Whatsapp.Template.Client.Requests;
using Whatsapp.Template.Client.Responses;
using Whatsapp.Template.Entities;
using Whatsapp.Template.Persistence.Contracts.Repositories;
using static Whatsapp.Template.Common.Enums.Enums;

namespace Whatsapp.Template.Business.Contracts.Service
{
    public class TemplateService : CommonBase, ITemplateService
    {
        #region Fields
        private readonly ITemplateEngine _templateEngine;
        private readonly ITemplateWhatsappRepository _templateWhatsappRepository;
        #endregion Fields
        #region Constructor
        public TemplateService(ILogAdapter LogAdapter, IJsonAdapter JsonAdapter, ITemplateEngine templateEngine, ITemplateWhatsappRepository templateWhatsappRepository) : base(LogAdapter, JsonAdapter)
        {
            _templateEngine = templateEngine;
            _templateWhatsappRepository = templateWhatsappRepository;
        }
        #endregion Constructor
        #region Public Methods

        public ClientResponse AddTemplate(TemplateRequestInfo templateRequestInfo)
        {
            ValidationTemplateResponseInfo result = _templateEngine.AddTemplate(templateRequestInfo);


            return result.IsValid == true ? Success()  : BadOperation(result.Message);
       
     
        }

        public ClientResponse GetTemplateTypeBody()
        {
            var analyzerType = Enum.GetValues(typeof(TemplateTypeBody))
                        .Cast<TemplateTypeBody>()
                        .Select(t => new 
                        {
                            Id = ((int)t),
                            Name = t.ToString()
                        });
            PagingResponse pagingResponse = new PagingResponse()
            {

                data = analyzerType
            };

            return Success(pagingResponse);
        }

        public ClientResponse GetTemplateHeaderMediaType()
        {
            var analyzerType = Enum.GetValues(typeof(TemplateHeaderMediaType))
                        .Cast<TemplateHeaderMediaType>()
                        .Select(t => new
                        {
                            Id = ((int)t),
                            Name = t.ToString()
                        });
            PagingResponse pagingResponse = new PagingResponse()
            {

                data = analyzerType
            };

            return Success(pagingResponse);
        }

        public ClientResponse GetTemplateButtonType()
        {
            var analyzerType = Enum.GetValues(typeof(TemplateButtonType))
                        .Cast<TemplateButtonType>()
                        .Select(t => new
                        {
                            Id = ((int)t),
                            Name = t.ToString()
                        });
            PagingResponse pagingResponse = new PagingResponse()
            {

                data = analyzerType
            };

            return Success(pagingResponse);
        }

        public ClientResponse GetCategory()
        {
            var analyzerType = Enum.GetValues(typeof(Category))
                        .Cast<Category>()
                        .Select(t => new
                        {
                            Id = ((int)t),
                            Name = t.ToString()
                        });
            PagingResponse pagingResponse = new PagingResponse()
            {

                data = analyzerType
            };

            return Success(pagingResponse);
        }

        public ClientResponse GetLanguage()
        {
            var list = _templateEngine.GetLanguage();
            //  var result = _autoMapper.Mapper.Map<IEnumerable<GetSummaryReportResponseInfo>>(list);
            return Success(list);
        }

        public ClientResponse GetTemplate(TemplateGetRequestInfo templateGetRequestInfo)
        {
            var list = _templateEngine.GetTemplate(templateGetRequestInfo);
            return Success(list);
        }
        public ClientResponse DeleteTemplate(TemplateDeleteRequestInfo templateDeleteRequestInfo)
        {
            ValidationTemplateResponseInfo result = _templateEngine.DeleteTemplate(templateDeleteRequestInfo);
            //  var result = _autoMapper.Mapper.Map<IEnumerable<GetSummaryReportResponseInfo>>(list);
            return result.IsValid == true ? Success() : BadOperation(result.Message);

        }
        public  ClientResponse UploadFile(IFormFile file)
        {

            Task<string> link = WriteFile(file);
            if (string.IsNullOrEmpty(link.Result))
            {
                return BadOperation("error on upload link");
            }
            return Success(link.Result);

        }

        public ClientResponse UploadWhatsappFile(IFormFile file,int tenantId, int accountId)
        {
            string link = WriteFileWhatsapp(file, tenantId, accountId);
            if (string.IsNullOrEmpty(link))
            {
                return BadOperation("error on upload link");
            }
            return Success(link);
        }
        #endregion Public Methods

        #region Private Methods
        private  string WriteFileWhatsapp(IFormFile file,int tenantId,int accountId)
        {
            WhatsappAccount whatsappAccount = _templateWhatsappRepository.GetWhatsappAccount(tenantId, accountId);

            string whatsappUploadFileUrl = Environment.GetEnvironmentVariable("WhatsappUploadFileBaseUrl");

            whatsappUploadFileUrl += "?access_token=" + whatsappAccount.Token + "&";
            whatsappUploadFileUrl += "file_length=" + file.Length + "&";
            whatsappUploadFileUrl += "file_type=" + file.ContentType + "&";
            whatsappUploadFileUrl += "file_name=" + file.FileName;

           string id = _templateEngine.GetUploadSession(whatsappUploadFileUrl);



            HttpContent fileStreamContent = new StreamContent(file.OpenReadStream());

            string link = _templateEngine.GetUploadFile(id, file, tenantId, accountId);
            return link;
        }



        private async Task<string> WriteFile(IFormFile file)
        {

            string firebaseApiKey = Environment.GetEnvironmentVariable("firebaseApiKey");

            string firebaseUserName = Environment.GetEnvironmentVariable("firebaseUserName");

            string firebasePassword = Environment.GetEnvironmentVariable("firebasePassword");

            string firebaseBucket = Environment.GetEnvironmentVariable("firebaseBucket");

            string fileName;
            var link = "";
            try
            {
                var extension = "." + file.FileName.Split('.')[file.FileName.Split(".").Length - 1];
                fileName = DateTime.Now.Ticks + extension;
                var pathBuilt = Path.Combine(Directory.GetCurrentDirectory(), "Upload\\files");
                if (!Directory.Exists(pathBuilt))
                {
                    Directory.CreateDirectory(pathBuilt);
                }
                var path = Path.Combine(Directory.GetCurrentDirectory(), "Upload\\files", fileName);

                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }

                var fs = new FileStream(path, FileMode.Open);

                var auth = new FirebaseAuthProvider(new FirebaseConfig(firebaseApiKey));
                var a = await auth.SignInWithEmailAndPasswordAsync(firebaseUserName, firebasePassword);
                var cancellation = new CancellationTokenSource();
                var upload = new FirebaseStorage(
                   firebaseBucket,
                   new FirebaseStorageOptions
                   {
                       AuthTokenAsyncFactory = () => Task.FromResult(a.FirebaseToken),
                       ThrowOnCancel = true
                   })
                    .Child("assets")
                    .Child($"{file.FileName}.{Path.GetExtension(file.FileName).Substring(1)}")
                    .PutAsync(fs, cancellation.Token);
                try
                {
                    link = await upload;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error on upload");
                }

            }
            catch (Exception ex)
            {

            }
            return link;
        }
        #endregion Private Methods
    }
}
