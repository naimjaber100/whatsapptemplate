﻿using Com.Monty.Omni.Global.Business.Engines;
using Common.Business.Common;
using Common.Client.Response;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using Whatsapp.Template.AutoMapper;
using Whatsapp.Template.Business.Contracts.Engine;
using Whatsapp.Template.Client.Requests;
using Whatsapp.Template.Client.Responses;
using Whatsapp.Template.Entities;
using Whatsapp.Template.Persistence.Contracts.Repositories;
using static Whatsapp.Template.Client.Responses.TemplateTypeResponseInfo;
using static Whatsapp.Template.Common.Enums.Enums;

namespace Whatsapp.Template.Business.Engine
{
    public class TemplateEngine: EngineBase,ITemplateEngine
    {
        #region Fields
        private readonly ITemplateWhatsappRepository _templateRepository;
        private readonly IHttpClientAdapter _httpClientAdapter;
        private readonly IAutoMapper _autoMapper;
        #endregion Fields

        #region Constructor
        public TemplateEngine(ITemplateWhatsappRepository templateRepository, IHttpClientAdapter httpClientAdapter,
            IServiceProvider serviceProvider, IAutoMapper autoMapper) : base(serviceProvider)
        {
            _templateRepository = templateRepository;
            _httpClientAdapter = httpClientAdapter;
            _autoMapper = autoMapper;


        }

        #endregion Constructor

        #region Public Methods
        public IEnumerable<Language> GetLanguage()
        {
            return _templateRepository.GetLanguage();
        }

        public ValidationTemplateResponseInfo AddTemplate(TemplateRequestInfo templateRequestInfo)
        {
            TemplateResponseInfo templateResponseInfo = new TemplateResponseInfo();

            ValidationTemplateResponseInfo validationTemplateResponseInfo = new ValidationTemplateResponseInfo();
            validationTemplateResponseInfo = ValidateTemplate(templateRequestInfo);
            WhatsappAccount whatsappAccount = _templateRepository.GetWhatsappAccount(templateRequestInfo.TenantId, templateRequestInfo.AccountId);
 
            if (validationTemplateResponseInfo.IsValid && whatsappAccount != null)
            {
               
            List<object> list = new List<object>();
            List<object> listButton = new List<object>();

          

            if (templateRequestInfo.headerTextRequestInfo != null)
            {
                    HeaderTextResponseInfo headerTextResponseInfo = _autoMapper.Mapper.Map<HeaderTextRequestInfo, HeaderTextResponseInfo>(templateRequestInfo.headerTextRequestInfo);

                    list.Add(headerTextResponseInfo);

            }
            if (templateRequestInfo.headerMediaRequestInfo != null)
                {
                    for (int i = 0; i < templateRequestInfo.headerMediaRequestInfo.example.header_handle.Count; i++)
                    {
                        templateRequestInfo.headerMediaRequestInfo.example.header_handle[i] = templateRequestInfo.headerMediaRequestInfo.example.header_handle[i].Replace("&", "%26");
                    }

                    HeaderMediaResponseInfo headerMediaResponseInfo = _autoMapper.Mapper.Map<HeaderMediaRequestInfo, HeaderMediaResponseInfo>(templateRequestInfo.headerMediaRequestInfo);

                    list.Add(headerMediaResponseInfo);

            }

            BodyResponseInfo bodyResponseInfo = _autoMapper.Mapper.Map<BodyRequestInfo, BodyResponseInfo>(templateRequestInfo.bodyRequestInfo);

            list.Add(bodyResponseInfo);

                if (templateRequestInfo.footerRequestInfo != null)
            {
                    FooterResponseInfo footerResponseInfo = _autoMapper.Mapper.Map<FooterRequestInfo, FooterResponseInfo>(templateRequestInfo.footerRequestInfo);

                    list.Add(footerResponseInfo);

            }
            if (templateRequestInfo.buttonBody != null)
            {
                if(templateRequestInfo.buttonBody.buttonUrlRequestInfo != null)
                {
                        ButtonUrlResponseInfo buttonUrlResponseInfo = _autoMapper.Mapper.Map<ButtonUrlRequestInfo, ButtonUrlResponseInfo>(templateRequestInfo.buttonBody.buttonUrlRequestInfo);

                        listButton.Add(buttonUrlResponseInfo);
                }
                if (templateRequestInfo.buttonBody.buttonCallRequestInfo != null)
                {
                        ButtonCallResponseInfo buttonCallResponseInfo = _autoMapper.Mapper.Map<ButtonCallRequestInfo, ButtonCallResponseInfo>(templateRequestInfo.buttonBody.buttonCallRequestInfo);

                        listButton.Add(buttonCallResponseInfo);
                }
                    ButtonObject buttonObject = new ButtonObject();
                    buttonObject.type = "BUTTONS";
                    buttonObject.buttons = listButton;

                list.Add(buttonObject);
            }


            string whatsappTemplateBaseUrl = Environment.GetEnvironmentVariable("WhatsappTemplateBaseUrl");
            string WhatsappMessageTemplate = Environment.GetEnvironmentVariable("WhatsappMessageTemplate");
   
            string addTemplateUrl = $"{whatsappTemplateBaseUrl}{whatsappAccount.WhatsappAccountBusinessId}{WhatsappMessageTemplate}";
         
                
                // string addTemplateUrl = "https://graph.facebook.com/v13.0/108593588528560/message_templates?";

            string Components = _jsonAdapter.Serialize(list);

            addTemplateUrl += "?name=" + templateRequestInfo.Name + "&";
            addTemplateUrl += "language=" + templateRequestInfo.Language + "&";
            addTemplateUrl += "category=" + templateRequestInfo.Category + "&";
            addTemplateUrl += "components=" + Components + "&";
            addTemplateUrl += "access_token=" + whatsappAccount.Token;


            var task = SentTemplate(addTemplateUrl);
            task.Wait();

            var result = task.Result;

                if (result.StatusCode == "OK")
                    {
                        int TemplateId =  AddTemplateData(templateRequestInfo);
                        if(TemplateId >0)
                        {
                            int TempParameterId = AddTemplateParameterData(templateRequestInfo, TemplateId);
                        }
                    }
                else
                {

                    object cr = _jsonAdapter.Deserialize<object>(result.Message.ToString());


                    var dataError = _jsonAdapter.Deserialize<ErrorsMessage>(cr.ToString());
                    var errorList = _jsonAdapter.Deserialize<ErrorList>(dataError.Error.ToString());
                    validationTemplateResponseInfo.IsValid = false;
                    validationTemplateResponseInfo.Message = errorList.error_user_title + " " + errorList.message;
                }

            }
            return validationTemplateResponseInfo;
           // return templateResponseInfo;
        }

        public class DataObject
        {
            public object data { get; set; }
            

        }
        public class GetObject
        {
            public string name { get; set; }

            public string language { get; set; }
            public string status { get; set; }
            public string category { get; set; }
            public string id { get; set; }
            public List<BodyRequest> components { get; set; }

        }

        public class BodyRequest
        {
            public string type { get; set; }
            public string format { get; set; }
            public string text { get; set; }
            public  List<ButtonBodyGet> buttons { get; set; }
        }

        public class ButtonBodyGet
        {
            public string? type { get; set; }

            public string? text { get; set; }

            public string? phone_number { get; set; }
            public string? url { get; set; }
        }
        public object GetTemplate(TemplateGetRequestInfo templateGetRequestInfo)
        {

            WhatsappAccount whatsappAccount = _templateRepository.GetWhatsappAccount(templateGetRequestInfo.TenantId, templateGetRequestInfo.AccountId);
            var List =new object();
            if(whatsappAccount != null)
            {
                string whatsappTemplateBaseUrl = Environment.GetEnvironmentVariable("WhatsappTemplateBaseUrl");
                string WhatsappMessageTemplate = Environment.GetEnvironmentVariable("WhatsappMessageTemplate");

                string GetTemplateUrl = $"{whatsappTemplateBaseUrl}{whatsappAccount.WhatsappAccountBusinessId}{WhatsappMessageTemplate}";
                GetTemplateUrl += "?access_token=" + whatsappAccount.Token + "&";
                GetTemplateUrl += "status=" + templateGetRequestInfo.Status + "&";
                GetTemplateUrl += "name=" + templateGetRequestInfo.Name + "&" ;
                GetTemplateUrl += "limit=" + templateGetRequestInfo.Limit;

                var task = GetTemplateWhatsapp(GetTemplateUrl);
                task.Wait();
                var result = task.Result;
                if (result.StatusCode == "OK")
                {
                    object cr = _jsonAdapter.Deserialize<object>(result.Message.ToString());


                    var Data = _jsonAdapter.Deserialize<DataObject>(cr.ToString());
                    List = _jsonAdapter.Deserialize<List<GetObject>>(Data.data.ToString());


                    return List;
                }
            }
            return List; 
        }

        public ValidationTemplateResponseInfo DeleteTemplate(TemplateDeleteRequestInfo templateDeleteRequestInfo)
        {
            ValidationTemplateResponseInfo validationTemplateResponseInfo = new ValidationTemplateResponseInfo();
            WhatsappAccount whatsappAccount = _templateRepository.GetWhatsappAccount(templateDeleteRequestInfo.TenantId, templateDeleteRequestInfo.AccountId);

            logger.WriteInfoLog("start delete template engine");
            var List = new object();
            if (whatsappAccount != null)
            {
                string whatsappTemplateBaseUrl = Environment.GetEnvironmentVariable("WhatsappTemplateBaseUrl");
                string WhatsappMessageTemplate = Environment.GetEnvironmentVariable("WhatsappMessageTemplate");

                string DeleteTemplateUrl = $"{whatsappTemplateBaseUrl}{whatsappAccount.WhatsappAccountBusinessId}{WhatsappMessageTemplate}";
                DeleteTemplateUrl += "?access_token=" + whatsappAccount.Token + "&";
                DeleteTemplateUrl += "name=" + templateDeleteRequestInfo.Name;

                logger.WriteInfoLog(string.Format("DeleteTemplateUrl  {0}", DeleteTemplateUrl));

                logger.WriteInfoLog("start delete template engine");
                var task = DeleteTemplateWhatsapp(DeleteTemplateUrl);
                task.Wait();
                var result = task.Result;
              
                if (result.StatusCode == "OK")
                {
                    validationTemplateResponseInfo.IsValid = true;
                    validationTemplateResponseInfo.Message = "Success";
                }

                else
                {

                    object cr = _jsonAdapter.Deserialize<object>(result.Message.ToString());


                    var dataError = _jsonAdapter.Deserialize<ErrorsMessage>(cr.ToString());
                    var errorList = _jsonAdapter.Deserialize<ErrorList>(dataError.Error.ToString());
                    validationTemplateResponseInfo.IsValid = false;
                    validationTemplateResponseInfo.Message = errorList.error_user_title + " " + errorList.message;
                }
            }
            return validationTemplateResponseInfo;
        }



        public  string GetUploadSession(string UploadTokenUrl)
        {
            string upload = "";
            var task = SendUpoadSession(UploadTokenUrl);
            task.Wait();
            var result = task.Result;
            if(result.StatusCode == "OK")
            {
                UploadSession o  = _jsonAdapter.Deserialize<UploadSession>(result.Message.ToString());
                upload = o.id;
            }
            return upload;
        }

        public string GetUploadFile(string Id, IFormFile file,int tenantId, int accountId)
        {


            string whatsappTemplateBaseUrl = Environment.GetEnvironmentVariable("WhatsappTemplateBaseUrl");
           // string WhatsappMessageTemplate = Environment.GetEnvironmentVariable("WhatsappMessageTemplate");

            string UploadFileUrl = $"{whatsappTemplateBaseUrl}{Id}";

            WhatsappAccount whatsappAccount = _templateRepository.GetWhatsappAccount(tenantId, accountId);
            string upload = "";
            var task = SendUpoadFile(UploadFileUrl, file, whatsappAccount.Token);
            task.Wait();
            var result = task.Result;
            if (result.StatusCode == "OK")
            {
                UploadSession o = _jsonAdapter.Deserialize<UploadSession>(result.Message.ToString());
                upload = o.h;
            }
            return upload;
        }


        #endregion Public Methods
        #region Private Methods

        private static async Task<TemplateResponseInfo> SendUpoadSession(string UploadTokenUrl)
        {
            TemplateResponseInfo templateResponseInfo = new TemplateResponseInfo();
            var http = new HttpClient();
            HttpResponseMessage response = await http.PostAsync(UploadTokenUrl, null);

            string responseBody = await response.Content.ReadAsStringAsync();
            templateResponseInfo.StatusCode = response.StatusCode.ToString();
            templateResponseInfo.Message = responseBody;

            return templateResponseInfo;
        }
        private static async Task<TemplateResponseInfo> SendUpoadFile(string UploadFileUrl, IFormFile file, string token)
        {

           
            TemplateResponseInfo templateResponseInfo = new TemplateResponseInfo();
            var http = new HttpClient();


            http.DefaultRequestHeaders.Accept.Clear();
            http.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("OAuth", token);
           
            http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", file.ContentType);
            http.DefaultRequestHeaders.TryAddWithoutValidation("file_offset", "0");
            http.DefaultRequestHeaders.TryAddWithoutValidation("Host", "graph.facebook.com");
            http.DefaultRequestHeaders.TryAddWithoutValidation("Connection", "close");

            HttpContent fileStreamContent = new StreamContent(file.OpenReadStream());
            fileStreamContent.Headers.ContentType = new MediaTypeHeaderValue(file.ContentType);
          //  multipartFormContent.Add(fileStreamContent, name: "file", fileName: file.FileName);

          

            HttpResponseMessage response = await http.PostAsync(UploadFileUrl, fileStreamContent);

            string responseBody = await response.Content.ReadAsStringAsync();
            templateResponseInfo.StatusCode = response.StatusCode.ToString();
            templateResponseInfo.Message = responseBody;

            return templateResponseInfo;
        }
        private ValidationTemplateResponseInfo ValidateTemplate(TemplateRequestInfo templateRequestInfo)
        {
            ValidationTemplateResponseInfo templateResponseInfo = new ValidationTemplateResponseInfo();

            templateResponseInfo = CheckValidateBody(templateRequestInfo.bodyRequestInfo);

            if (templateResponseInfo.IsValid)
            {
                if(templateRequestInfo.footerRequestInfo != null)
                {
                    templateResponseInfo = CheckValidateFooter(templateRequestInfo.footerRequestInfo);

                }
            }

            if (templateResponseInfo.IsValid)
            {
                if(templateRequestInfo.headerMediaRequestInfo != null)
                {
                    templateResponseInfo = CheckValidateHeaderMedia(templateRequestInfo.headerMediaRequestInfo);

                }
            }
            if (templateResponseInfo.IsValid)
            {
                if(templateRequestInfo.headerTextRequestInfo != null)
                {
                    templateResponseInfo = CheckValidateHeaderText(templateRequestInfo.headerTextRequestInfo);

                }
            }

            if (templateResponseInfo.IsValid)
            {
                if(templateRequestInfo.buttonBody != null && templateRequestInfo.buttonBody.buttonCallRequestInfo != null)
                {
                    templateResponseInfo = CheckValidateTextCall(templateRequestInfo.buttonBody.buttonCallRequestInfo);

                }
            }

            if (templateResponseInfo.IsValid)
            {
                if(templateRequestInfo.buttonBody != null && templateRequestInfo.buttonBody.buttonUrlRequestInfo != null)
                {
                    templateResponseInfo = CheckValidateTextUrl(templateRequestInfo.buttonBody.buttonUrlRequestInfo);

                }
            }         
            return templateResponseInfo;
        }
  
        private static async Task<TemplateResponseInfo> SentTemplate(string TemplateUrl)
        {
            TemplateResponseInfo templateResponseInfo = new TemplateResponseInfo();
            var http = new HttpClient();
            HttpResponseMessage response = await http.PostAsync(TemplateUrl, null);

            string responseBody = await response.Content.ReadAsStringAsync();
            templateResponseInfo.StatusCode = response.StatusCode.ToString();
            templateResponseInfo.Message = responseBody;

            return templateResponseInfo;
        }

        private int AddTemplateData(TemplateRequestInfo templateRequestInfo)
        {
            int result = _templateRepository.AddTemplate(templateRequestInfo);

            return result;
        }

        private int AddTemplateParameterData(TemplateRequestInfo templateRequestInfo,int TemplateId)
        {
            if(templateRequestInfo.headerMediaRequestInfo != null)
            {
                int result = _templateRepository.AddHeaderMediaTemplateParamter(templateRequestInfo.headerMediaRequestInfo, TemplateId);

            }
            if (templateRequestInfo.headerTextRequestInfo != null)
            {
                int result = _templateRepository.AddHeaderTextTemplateParamter(templateRequestInfo.headerTextRequestInfo, TemplateId);

            }
            if (templateRequestInfo.bodyRequestInfo != null)
            {
                int result = _templateRepository.AddBodyTemplateParamter(templateRequestInfo.bodyRequestInfo, TemplateId);

            }
            if (templateRequestInfo.footerRequestInfo != null)
            {
                int result = _templateRepository.AddFooterTemplateParamter(templateRequestInfo.footerRequestInfo, TemplateId);

            }

            if (templateRequestInfo.buttonBody != null)
            {
                if(templateRequestInfo.buttonBody.buttonCallRequestInfo != null)
                {
                    int result = _templateRepository.AddButtonCallTemplateParamter(templateRequestInfo.buttonBody.buttonCallRequestInfo, TemplateId);
                }
                if (templateRequestInfo.buttonBody.buttonUrlRequestInfo != null)
                {
                    int result = _templateRepository.AddButtonUrlTemplateParamter(templateRequestInfo.buttonBody.buttonUrlRequestInfo, TemplateId);
                }

            }

            return 1;
        }

        private static async Task<TemplateResponseInfo> GetTemplateWhatsapp(string GetTemplateUrl)
        {
            TemplateResponseInfo templateResponseInfo = new TemplateResponseInfo();
            CancellationToken cancellationToken = new CancellationToken();
            var http = new HttpClient();

            HttpResponseMessage response = await http.GetAsync(GetTemplateUrl, cancellationToken);
            string responseBody = await response.Content.ReadAsStringAsync();

            templateResponseInfo.StatusCode = response.StatusCode.ToString();
            templateResponseInfo.Message = responseBody;



            //  var data = _jsonAdapter.Deserialize<PagingResponse>(cr.Data.ToString()).data.ToList();

            return templateResponseInfo;
            // return responseBody;
        }
        private static async Task<TemplateResponseInfo> DeleteTemplateWhatsapp(string DeleteTemplateUrl)
        {
            TemplateResponseInfo templateResponseInfo = new TemplateResponseInfo();
            CancellationToken cancellationToken = new CancellationToken();
            var http = new HttpClient();

            HttpResponseMessage response = await http.DeleteAsync(DeleteTemplateUrl, cancellationToken);
            string responseBody = await response.Content.ReadAsStringAsync();

            templateResponseInfo.StatusCode = response.StatusCode.ToString();
            templateResponseInfo.Message = responseBody;

            return templateResponseInfo;
        }

        private ValidationTemplateResponseInfo CheckValidateBody(BodyRequestInfo bodyRequestInfo)
        {
            ValidationTemplateResponseInfo templateResponseInfo = new ValidationTemplateResponseInfo();

            if (bodyRequestInfo != null)
            {
               if(bodyRequestInfo.type != TemplateTypeBody.BODY)
                {
                    templateResponseInfo.IsValid = false;
                    templateResponseInfo.Message = "incorrect Body format type";
                }

               else if (bodyRequestInfo.example != null && bodyRequestInfo.example.body_text != null &&  bodyRequestInfo.example.body_text[0].Count != bodyRequestInfo.numberOfVariables)
                {
                    templateResponseInfo.IsValid = false;
                    templateResponseInfo.Message = "Body varibale should enter all example";
                }
                else
                {
                    templateResponseInfo.IsValid = true;
                    templateResponseInfo.Message = "Success";
                }
            }
            else
            {
                templateResponseInfo.IsValid = false;
                templateResponseInfo.Message = "Body field is required";

            }
            return templateResponseInfo;

        }

        private ValidationTemplateResponseInfo CheckValidateFooter(FooterRequestInfo footerRequestInfo)
        {
            ValidationTemplateResponseInfo templateResponseInfo = new ValidationTemplateResponseInfo();

            if (footerRequestInfo != null)
            {
                if (footerRequestInfo.type != TemplateTypeBody.FOOTER)
                {
                    templateResponseInfo.IsValid = false;
                    templateResponseInfo.Message = "incorrect Footer format type";
                }
                else if (string.IsNullOrEmpty(footerRequestInfo.text))
                {
                    templateResponseInfo.IsValid = false;
                    templateResponseInfo.Message = "text footer is required";
                }


                else
                {
                    templateResponseInfo.IsValid = true;
                    templateResponseInfo.Message = "Success";
                }
            }
            else
            {
                templateResponseInfo.IsValid = true;
                templateResponseInfo.Message = "Success";

            }
            return templateResponseInfo;

        }

        private ValidationTemplateResponseInfo CheckValidateHeaderText(HeaderTextRequestInfo headerTextRequestInfo)
        {
            ValidationTemplateResponseInfo templateResponseInfo = new ValidationTemplateResponseInfo();

            if (headerTextRequestInfo != null)
            {
                if (headerTextRequestInfo.type != TemplateTypeBody.HEADER)
                {
                    templateResponseInfo.IsValid = false;
                    templateResponseInfo.Message = "incorrect Header Text format type";
                }
               else if (headerTextRequestInfo.format != TemplateHeaderMediaType.TEXT)
                {
                    templateResponseInfo.IsValid = false;
                    templateResponseInfo.Message = "incorrect Header  format type";
                }

                else if (headerTextRequestInfo.example != null && headerTextRequestInfo.example.header_text != null && headerTextRequestInfo.example.header_text.Count != headerTextRequestInfo.numberOfVariables)
                {
                    templateResponseInfo.IsValid = false;
                    templateResponseInfo.Message = "Header text should enter all  example";
                }
                else
                {
                    templateResponseInfo.IsValid = true;
                    templateResponseInfo.Message = "Success";
                }
            }
            else
            {
                templateResponseInfo.IsValid = true;
                templateResponseInfo.Message = "Success";

            }
            return templateResponseInfo;

        }
        private ValidationTemplateResponseInfo CheckValidateTextUrl(ButtonUrlRequestInfo buttonUrlRequestInfo)
        {
            ValidationTemplateResponseInfo templateResponseInfo = new ValidationTemplateResponseInfo();

            if(buttonUrlRequestInfo != null)
            {
                if(string.IsNullOrEmpty(buttonUrlRequestInfo.text))
                {
                    templateResponseInfo.IsValid = false;
                    templateResponseInfo.Message = "Button Text Url field is required";
                }
                else if (string.IsNullOrEmpty(buttonUrlRequestInfo.url))
                {
                    templateResponseInfo.IsValid = false;
                    templateResponseInfo.Message = "Url field is required";
                }
                else if (buttonUrlRequestInfo.type != TemplateButtonType.URL)
                {
                    templateResponseInfo.IsValid = false;
                    templateResponseInfo.Message = "Incorect format type Url";
                }

                //else if (!IsValidURL (buttonUrlRequestInfo.url))
                //{
                //    templateResponseInfo.IsValid = false;
                //    templateResponseInfo.Message = "The url is not valid";
                //}
                else
                {
                    templateResponseInfo.IsValid = true;
                    templateResponseInfo.Message = "Success";
                }
            }
            else
            {
                templateResponseInfo.IsValid = true;
                templateResponseInfo.Message = "Success";

            }
            return templateResponseInfo;

        }

        private ValidationTemplateResponseInfo CheckValidateTextCall(ButtonCallRequestInfo buttonCallRequestInfo)
        {
            ValidationTemplateResponseInfo templateResponseInfo = new ValidationTemplateResponseInfo();

            if (buttonCallRequestInfo != null)
            {
                if (string.IsNullOrEmpty(buttonCallRequestInfo.text))
                {
                    templateResponseInfo.IsValid = false;
                    templateResponseInfo.Message = "Button Text Url field is required";
                }
                else if (string.IsNullOrEmpty(buttonCallRequestInfo.phone_number))
                {
                    templateResponseInfo.IsValid = false;
                    templateResponseInfo.Message = "Phone Number field is required";
                }
                else if (buttonCallRequestInfo.type != TemplateButtonType.PHONE_NUMBER)
                {
                    templateResponseInfo.IsValid = false;
                    templateResponseInfo.Message = "Incorect format type Phone number";
                }
                else if(!Int64.TryParse(buttonCallRequestInfo.phone_number, out long n))
                {
                    templateResponseInfo.IsValid = false;
                    templateResponseInfo.Message = "Button Phone Number should be integer";
                }
                else if (buttonCallRequestInfo.phone_number.Length < 8  || buttonCallRequestInfo.phone_number.Length > 14)
                {
                    templateResponseInfo.IsValid = false;
                    templateResponseInfo.Message = "Phone Number should be between 8 and 14 digits";
                }

                else
                {
                    templateResponseInfo.IsValid = true;
                    templateResponseInfo.Message = "Success";
                }
            }
            else
            {
                templateResponseInfo.IsValid = true;
                templateResponseInfo.Message = "Success";

            }
            return templateResponseInfo;

        }

        private ValidationTemplateResponseInfo CheckValidateHeaderMedia( HeaderMediaRequestInfo headerMediaRequestInfo)
        {
            ValidationTemplateResponseInfo templateResponseInfo = new ValidationTemplateResponseInfo();

            if (headerMediaRequestInfo != null)
            {
                if (headerMediaRequestInfo.type != TemplateTypeBody.HEADER)
                {
                    templateResponseInfo.IsValid = false;
                    templateResponseInfo.Message = "incorrect Header format type";
                }
               else if (!Enum.GetNames(typeof(TemplateHeaderMediaType)).Any(x => x.ToLower() == headerMediaRequestInfo.format.ToString().ToLower()))
                {
                    templateResponseInfo.IsValid = false;
                    templateResponseInfo.Message = "format header type is invalid";
                }

                else if (headerMediaRequestInfo.example == null || headerMediaRequestInfo.example.header_handle == null)
                {
                    templateResponseInfo.IsValid = false;
                    templateResponseInfo.Message = "Header Media should enter an example";
                }
                //else if (headerMediaRequestInfo.format.ToString().ToLower() != TemplateHeaderMediaType.ToString().ToLower())
                //{
                //    templateResponseInfo.IsValid = false;
                //    templateResponseInfo.Message = "format header type is invalid";
                //}
                else
                {
                    templateResponseInfo.IsValid = true;
                    templateResponseInfo.Message = "Success";
                }
            }
            else
            {
                templateResponseInfo.IsValid = true;
                templateResponseInfo.Message = "Success";

            }
            return templateResponseInfo;

        }

        private  bool IsValidURL(string URL)
        {
            string Pattern = @"^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$";
            Regex Rgx = new Regex(Pattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);
            return Rgx.IsMatch(URL);
        }
        #endregion Private Methods
    }
}
