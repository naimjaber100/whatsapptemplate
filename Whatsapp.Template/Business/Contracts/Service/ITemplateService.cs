﻿using Common.Client.Response;
using Whatsapp.Template.Client.Requests;

namespace Whatsapp.Template.Business.Contracts.Service
{
    public interface ITemplateService
    {
     
        ClientResponse AddTemplate(TemplateRequestInfo templateRequestInfo);

        ClientResponse GetTemplateTypeBody();

        ClientResponse GetTemplateHeaderMediaType();

        ClientResponse GetTemplateButtonType();
        ClientResponse GetCategory();

        ClientResponse GetLanguage();

        ClientResponse GetTemplate(TemplateGetRequestInfo templateGetRequestInfo);
        ClientResponse DeleteTemplate(TemplateDeleteRequestInfo templateDeleteRequestInfo);

        ClientResponse UploadFile(IFormFile file);
        ClientResponse UploadWhatsappFile(IFormFile file,int tenantId,int accountId);
    }
}
