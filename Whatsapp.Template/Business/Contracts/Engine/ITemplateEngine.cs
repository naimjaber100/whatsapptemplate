﻿using Whatsapp.Template.Client.Requests;
using Whatsapp.Template.Client.Responses;
using Whatsapp.Template.Entities;

namespace Whatsapp.Template.Business.Contracts.Engine
{
    public interface ITemplateEngine
    {
        // TemplateResponseInfo AddTextTemplate(TemplateTextRequestInfo templateTextRequestInfo);
       ValidationTemplateResponseInfo AddTemplate(TemplateRequestInfo templateRequestInfo);


        IEnumerable<Language> GetLanguage();

        object GetTemplate(TemplateGetRequestInfo templateGetRequestInfo);

        ValidationTemplateResponseInfo DeleteTemplate(TemplateDeleteRequestInfo templateDeleteRequestInfo);

        string GetUploadSession(string UploadTokenUrl);

        string GetUploadFile(string Id, IFormFile file, int tenantId, int accountId);
    }
}
