﻿using AutoMapper;
using Whatsapp.Template.Client.Requests;
using static Whatsapp.Template.Client.Responses.TemplateTypeResponseInfo;

namespace Whatsapp.Template.AutoMapper
{
    public class AutoMapperr : IAutoMapper
    {
        public IMapper Mapper { get; private set; }
        public AutoMapperr()
        {
            ConfigureMappers();
        }

        public void ConfigureMappers()
        {
            var config = new MapperConfiguration(cfg =>
            {
                //    cfg.CreateMap<CampaignValidateNumber, CampaignDetail>();

                cfg.CreateMap<HeaderTextRequestInfo, HeaderTextResponseInfo>();
                cfg.CreateMap<HeaderMediaRequestInfo, HeaderMediaResponseInfo>();
                cfg.CreateMap<HeaderExampleRequestInfo, HeaderExampleResponseInfo>();

                cfg.CreateMap<HeaderExampleTextRequestInfo, HeaderTextExampleResponseInfo>();

                cfg.CreateMap<BodyRequestInfo, BodyResponseInfo>();

                cfg.CreateMap<BodyTextRequestInfo, BodyTextResponseInfo>();

             //   cfg.CreateMap<BodyExampleRequestInfo, BodyExampleResponseInfo>();

                cfg.CreateMap<FooterRequestInfo, FooterResponseInfo>();
                cfg.CreateMap<ButtonCallRequestInfo, ButtonCallResponseInfo>();
                cfg.CreateMap<ButtonUrlRequestInfo, ButtonUrlResponseInfo>();

            });

            Mapper = config.CreateMapper();
            config.AssertConfigurationIsValid();
        }
    }


    public interface IAutoMapper
    {
        IMapper Mapper { get; }
    }
}
