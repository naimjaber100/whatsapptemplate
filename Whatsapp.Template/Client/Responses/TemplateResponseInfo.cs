﻿namespace Whatsapp.Template.Client.Responses
{
    public class TemplateResponseInfo
    {
        public string? StatusCode { get; set; }
        public string? Message { get; set; }
    }
}
