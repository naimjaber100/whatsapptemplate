﻿using System.ComponentModel.DataAnnotations;

namespace Whatsapp.Template.Client.Responses
{
    public class TemplateTypeResponseInfo
    {
        public class HeaderTextResponseInfo
        {
            public string? type { get; set; }
            public string? format { get; set; }
            public string? text { get; set; }
            public HeaderTextExampleResponseInfo? example { get; set; }
        }

        public class HeaderMediaResponseInfo
        {
            public string? type { get; set; }
            public string? format { get; set; }
            public HeaderExampleResponseInfo? example { get; set; }
        }

        public class HeaderTextExampleResponseInfo
        {
            public List<string>? header_text { get; set; }
        }

        public class HeaderExampleResponseInfo
        {
            public List<string>? header_handle { get; set; }
        }

        public class BodyResponseInfo
        {
       
            public string? type { get; set; }
        
            public string? text { get; set; }
         
            public BodyTextResponseInfo? example { get; set; }
        }

        public class BodyTextResponseInfo
        {
            public List<List<string>>? body_text { get; set; }
        }

     

        public class BodyExampleResponseInfo
        {
            public List<string>? text { get; set; }
        }

        public class FooterResponseInfo
        {
            public string? type { get; set; }
            public string? text { get; set; }
        }
   

        public class ButtonCallResponseInfo
        {
            public string? type { get; set; }
    
            public string? text { get; set; }

            public string? phone_number { get; set; }
        }

        public class ButtonUrlResponseInfo
        {
            public string? type { get; set; }
            public string? text { get; set; }
            public string? url { get; set; }
        }
    }
}
