﻿namespace Whatsapp.Template.Client.Responses
{
    public class ValidationTemplateResponseInfo
    {

        public bool IsValid { get;set;}
        public string? Message { get;set;}
    }
}
