﻿namespace Whatsapp.Template.Client.Responses
{
    public class ErrorTemplateResponseInfo
    {
        public string? Message { get; set; }
        public string? type { get; set; }
        public int Code { get; set; }
        public string? fbtrace_id { get; set; }
    }


    public class ErrorsMessage
    {
        public object? Error { get; set; }
    }

    public class ErrorList
    {

        public string? message { get; set; } = null;
        public string? error_user_title { get; set; }
        public string? error_user_msg { get; set; }
    }
}
