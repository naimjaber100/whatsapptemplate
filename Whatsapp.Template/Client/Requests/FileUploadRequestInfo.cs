﻿namespace Whatsapp.Template.Client.Requests
{
    public class FileUploadRequestInfo
    {
        public IFormFile? formFile { get; set; }
    }
}
