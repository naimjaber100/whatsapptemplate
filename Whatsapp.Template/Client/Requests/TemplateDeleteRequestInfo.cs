﻿namespace Whatsapp.Template.Client.Requests
{
    public class TemplateDeleteRequestInfo
    {
        public int AccountId { get; set; }
        public int TenantId { get; set; }
        public string? Name { get; set; }
    }
}
