﻿using System.ComponentModel.DataAnnotations;
using static Whatsapp.Template.Common.Enums.Enums;

namespace Whatsapp.Template.Client.Requests
{
    public class TemplateTypeRequestInfo
    {

    }

    public class HeaderTextRequestInfo
    {
        public TemplateTypeBody? type { get; set; }
        public TemplateHeaderMediaType? format { get; set; }
        [MaxLength(60, ErrorMessage = "header text  cannot be longer than 60 characters.")]
        public string? text { get; set; }
        public virtual int? numberOfVariables { get; set; }
        public HeaderExampleTextRequestInfo? example { get; set; }
    }

    public class HeaderMediaRequestInfo
    {
        public TemplateTypeBody? type { get; set; }
        public TemplateHeaderMediaType? format { get; set; }
        public HeaderExampleRequestInfo? example { get; set; }
    }

    public class HeaderExampleTextRequestInfo
    {
        public List<string>? header_text { get; set; }
    }

    public class HeaderExampleRequestInfo
    {
        public List<string>? header_handle { get; set; }
    }

    public class BodyRequestInfo
    {
        [Required]
        public TemplateTypeBody? type { get; set; }
        [Required]
        [MaxLength(512, ErrorMessage = "body text  cannot be longer than 1024 characters.")]
        public string? text { get; set; }

        public virtual int? numberOfVariables { get; set; }

        public BodyTextRequestInfo? example { get; set; }
    }

    public class BodyTextRequestInfo
    {
        public List< List<string>>? body_text { get; set; }
    }

 
    public class FooterRequestInfo
    {
        public TemplateTypeBody? type { get; set; }
        [MaxLength(60, ErrorMessage = "footer text  cannot be longer than 60 characters.")]
        public string? text { get; set; }
    }
    public class ButtonBody
    {
       
        public ButtonCallRequestInfo? buttonCallRequestInfo { get; set; }
        public ButtonUrlRequestInfo? buttonUrlRequestInfo { get; set; }
    }

    public class ButtonCallRequestInfo
    {
        public TemplateButtonType? type { get; set; }
        [MaxLength(25, ErrorMessage = "call text  cannot be longer than 25 characters.")]
        public string? text { get; set; }
        [MaxLength(20, ErrorMessage = "phone number cannot be longer than 20 characters.")]
        public string? phone_number { get; set; }
    }

    public class ButtonUrlRequestInfo
    {
        public TemplateButtonType? type { get; set; }
        [MaxLength(25, ErrorMessage = "url text  cannot be longer than 25 characters.")]
        public string? text { get; set; }
        public string? url { get; set; }

 
    }

   
}
