﻿namespace Whatsapp.Template.Client.Requests
{
    public class TemplateTextRequestInfo
    {
        public int AccountId { get; set; }

        public int TemplateType { get; set; }
        public string? Name { get; set; }
        public string? Language { get; set; }
        public string? Category { get; set; }
        public string? WABA { get; set; }
        public string? AccessToken { get;set; }
        public List<BodyRequestInfo>? Components { get; set; }
        public string? TemplateText { get; set; }
    }

    public class ComponentText
    {
        public string? type { get; set; }
        public string? text { get; set; }

    }


}
