﻿using System.ComponentModel.DataAnnotations;
using static Whatsapp.Template.Common.Enums.Enums;

namespace Whatsapp.Template.Client.Requests
{
    public class TemplateRequestInfo
    {
        public int AccountId { get; set; }

        public int TenantId { get; set; }

        public TemplateType TemplateType { get; set; }

        [Required]
        [RegularExpression(@"^[a-za-z0-9_]+$", ErrorMessage = "name should only contain lower characters,underscor,number")]
        [MaxLength(512, ErrorMessage = "Name cannot be longer than 512 characters.")]
        public string? Name { get; set; }
        [Required]
        public string? Language { get; set; }
        [Required]
        public Category? Category { get; set; }
        public string? WABA { get; set; }
        public string? AccessToken { get; set; }
        public string? TemplateText { get; set; }

        public HeaderMediaRequestInfo? headerMediaRequestInfo { get; set; }
        public HeaderTextRequestInfo? headerTextRequestInfo { get; set; }


        [Required(ErrorMessage = "Body template is required")]
        public BodyRequestInfo? bodyRequestInfo { get; set; }
        public FooterRequestInfo? footerRequestInfo { get; set; }


        public ButtonBody? buttonBody { get; set; }
        //public ButtonCallRequestInfo? buttonCallRequestInfo { get; set; }
        //public ButtonUrlRequestInfo? buttonUrlRequestInfo { get; set; }
    }
}
