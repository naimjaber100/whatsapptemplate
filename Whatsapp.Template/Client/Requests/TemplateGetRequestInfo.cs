﻿namespace Whatsapp.Template.Client.Requests
{
    public class TemplateGetRequestInfo
    {
        public int AccountId { get; set; }
        public int TenantId { get; set; }
        public string? Status { get; set; }
        public string? Name { get; set; }

        public int Limit { get; set; }
    }
}
